const express = require('express');
const app = express();
const port = 8003;
app.get('/', (req,res) => res.send('PARSE DOM APP'));
app.listen(port, () => console.log('App started on port ${port}...'));
